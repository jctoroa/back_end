
from rest_framework import serializers

from .models import Category, Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            "id",
            "name",
            "get_absolute_url",
            "description",
            "price",
            "get_image",
            "get_thumbnail"
        )
class ProductSerializerPost(serializers.ModelSerializer):          
    class Meta:
        model = Product
        fields = (
            "category",
            "name",
            "slug",
            "description",
            "price",
            "image",
            "thumbnail",
            "date_added"
        )
        def create(self, validated_data):
            productInstance = Product.objects.create(**validated_data)
            return productInstance
    
        def to_representation(self, obj):
            producto = Product.objects.get(id=obj.id)
            return {
                'category': producto.category,
                'name': producto.name,
                'slug':producto.slug,
                'description':producto.description,
                'price':producto.price,
                'image':producto.image,
                'thumbnail':producto.thumbnail,
                'date_added':producto.date_added
            }

class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Category
        fields = (
            "id",
            "name",
            "get_absolute_url",
            "products",
        )


