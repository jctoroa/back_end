from django.urls import path, include
from rest_framework.decorators import api_view

from product import views
from product.views import product_detail_api_view

urlpatterns = [
    path('latest-products/', views.LatestProductsList.as_view()),
    path('latest-products/<int:pk>/', product_detail_api_view, name = 'product_detail_api_view'),
    path('products/search/', views.search),
    path('products/<slug:category_slug>/<slug:product_slug>/',
         views.ProductDetail.as_view()),
    path('products/<slug:category_slug>/', views.CategoryDetail.as_view()),
]
